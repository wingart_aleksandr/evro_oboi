//----Include----
    if($(".owl_carousel").length){
      include("js/owl.carousel.js");
    }
    if($(".flexslider").length){
      include("js/jquery.flexslider.js");
    }
    if($(".data-mh").length){
      include("js/jquery.matchHeight-min.js");
    }
    if($(".open_popup").length){
      include("js/jquery.arcticmodal.js"); 
    }
    if($(".masked").length){
      include("js/maskedInput.js"); 
    };
    if($(".scroll-pane").length){
      include("js/jquery.jscrollpane.js"); 
    };
    if($(".scroll-mouse").length){
      include("js/jquery.mousewheel.js"); 
    };
    if($(".video_player").length){
      include("build/mediaelement-and-player.js"); 
    };
    if($(".superfish").length){
      include("js/superfish.js"); 
      include("js/hoverIntent.js"); 
    };
    if($(".wm-zoom-container").length){
      include("js/jquery.wm-zoom-1.0.js");

    };
      include("js/wingart.js");

//----Include-Function----
    function include(url){
      document.write('<script src="'+ url + '"></script>');
    }
//----Include end----

$(document).ready(function(){
    /*------Cкролл--------*/
    if ($('.scroll-mouse').length){
      $('.scroll-mouse').mousewheel();  
    };

    if ($('.scroll-pane').length){
      $('.scroll-pane').each(function(){
        var $this = $(this);
        if($this.hasClass('arr_top_down')){
          // console.log("yes");
          $this.jScrollPane({
            showArrows: true
          });
        }
        else{
          // console.log("no");
          $this.jScrollPane({
            showArrows: false
          });
        }
      });
    }; 
    // video player

    if($('.video_player').length){
      $('video').mediaelementplayer({
        success: function(player, node) {
          $('#' + node.id + '-mode').html('mode: ' + player.pluginType);
        }
      });
    }


    if($('.superfish').length){
      $('#superfish').superfish();
    }
    /*-----Выпадашка------*/

    if($('.accordion_item_link').length){
      $('.accordion_item_link').on('click', function(){
        $(this)
          .toggleClass('active')
          .next('.sub-menu')
          .slideToggle()
          .parents(".accordion_item")
          .siblings(".accordion_item")
          .find(".accordion_item_link")
          .removeClass("active")
          .next(".sub-menu")
          .slideUp();
      });  
    }
    // ===========================news_pagelist=========================== показ картинки при наведении

    $(".news_block_hover_list_item").hover(function(){

      if(!$(this).hasClass('active')){

          var src = $(this).find('.news_block_hover_main_show_img2').attr("src");

          $(".news_block_hover_list_item").removeClass('active');
          $(this).addClass("active");
          $('.news_block_hover_main_show_img').attr('src', src );
      }

    },
    function(){})
    // ===========================news_pagelist===========================  показ картинки при наведении

    if($(".wm-zoom-container").length){
      $('.wm-zoom-container').WMZoom({
        config : {
              stageW : 501,
              stageH : 453,
              inner  : false,
              position : 'right', 
              margin : 10
          }
      });
    };

    //--------------masked input start--------
    if($(".masked").length){

        $(function($){
          if( $("#date").length || $("#phone").length){
            $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
            $("#phone").mask("+7-***-***-**-**");
          }
        });

    };
    //--------------masked input end--------

    /*----flexslider-----*/

    // if($('.flexslider').length){
    //   $('.flexslider').each(function(){
    //     var $this = $(this);
    //     if($this.hasClass('thumbnails')){
    //       $this.flexslider({
    //         animation: "slide",
    //         minItems: 1, 
    //         maxItems: 1,
    //         direction: "horizontal",
    //         slideshow: false,
    //         controlNav: "thumbnails"
    //       });
    //     }
    //     else if($this.hasClass('vertical')){
    //       $this.flexslider({
    //         animation: "slide",
    //         slideshow: false,
    //         minItems: 1, 
    //         maxItems: 3,
    //         animationLoop: true,
    //         direction: "vertical"
    //       });
    //     }
    //     else if($this.hasClass('flexslider2')){
          
    //       $('#slider').flexslider({
    //         animation: "slide",
    //         controlNav: false,
    //         directionNav: false,
    //         itemMargin: 0,
    //         minItems: 1,
    //         maxItems: 1, 
    //         animationLoop: false,
    //         slideshow: false,
    //         sync: "#carousel"
    //       });

    //       $('#carousel').flexslider({
    //         animation: "slide",
    //         controlNav: false,
    //         animationLoop: false,
    //         slideshow: false,
    //         itemWidth: 117,
    //         // maxItems: 4, 
    //         itemMargin: 10,
    //         asNavFor: '#slider'
    //       });
         
    //     }
    //     else{
    //       $this.flexslider({
    //         direction: "horizontal",
    //         animation: "slide",
    //         minItems: 1
    //       });
    //     }
    //   });
    // };

    if($('.thumbnails').length){
      $('.thumbnails').flexslider({
        animation: "slide",
        minItems: 1, 
        maxItems: 1,
        direction: "horizontal",
        slideshow: false,
        controlNav: "thumbnails"
      });
      console.log("thumbnails");
    };

    if($('.vertical').length){
      $('.vertical').flexslider({
        animation: "slide",
        slideshow: false,
        minItems: 1, 
        maxItems: 3,
        animationLoop: true,
        direction: "vertical"
      });
      console.log("vertical");
    };

    if($('.flexslider2').length){
      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        directionNav: false,
        itemMargin: 0,
        minItems: 1,
        maxItems: 1, 
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });

      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 117,
        // maxItems: 4, 
        itemMargin: 10,
        asNavFor: '#slider'
      });
      console.log("sunchron");
    };

    if($('.flex_else').length){
      $('.flex_else').flexslider({
        direction: "horizontal",
        animation: "slide",
        minItems: 1
      });
      console.log("flex_else");
    };
    //----owl-carousel-----
 
    // if($('.owl_carousel').length){
    //   $('.owl_carousel').each(function(){
    //     var $this = $(this);
    //     if($this.hasClass('full_width')){
    //       $(this).owlCarousel({
    //         stopOnHover : true,
    //         rewindNav:false,
    //         navigation:true,
    //         paginationSpeed : 1000,
    //         goToFirstSpeed : 1000,
    //         singleItem : true,
    //         items : 1,
    //         loop: true,
    //         smartSpeed:1000,
    //         mouseDrag:true,
    //         autoplayTimeout:4000,
    //       });
    //     }
    //     else if($this.hasClass('vertical')){
    //       $(this).owlCarousel({
    //         animateOut: 'slideOutDown',
    //         animateIn: 'slideInDown',
    //         autoHeight:true,
    //         items:3,
    //         margin:0,
    //         navigation:true,
    //         loop: true,
    //         smartSpeed:450,
    //         // autoplay:true,
    //         // mouseDrag:false,
    //         touchDrag:false,
    //         autoplayTimeout: 2500
    //       });
    //     }
    //     else{
          // var itemA = $(this).attr('data-itemA'),
          //     itemB = $(this).attr('data-itemB'),
          //     itemC = $(this).attr('data-itemC'),
          //     itemD = $(this).attr('data-itemD'),
          //     itemE = $(this).attr('data-itemE');
          //     itemF = $(this).attr('data-itemF');
          //     itemG = $(this).attr('data-itemG');
    //       $(this).owlCarousel({
          //     nav: true ,
          //     mouseDrag:true,
          //     paginationSpeed : 1000,
          //     goToFirstSpeed : 1000,
          //     smartSpeed:500,
          //     responsive: {
          //         0 : {
          //              items: itemG
          //         },
          //         440 : {
          //              items: itemF
          //         },
          //         620 : {
          //              items: itemE
          //         },
          //         768 : {
          //              items: itemD
          //         },
          //         991 : {
          //              items: itemC
          //         },
          //         1199 : {
          //              items: itemB
          //         },
          //         1900 : {
          //              items: itemA
          //         }
          //     }
          // });
    //     }
    //   });
    // };

    if($('.vertical_owl').length){
      $('.vertical_owl').owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'slideInDown',
        autoHeight:true,
        items:3,
        margin:0,
        navigation:true,
        loop: true,
        smartSpeed:450,
        // autoplay:true,
        // mouseDrag:false,
        touchDrag:false,
        autoplayTimeout: 2500
      });
      console.log("vertical_owl");
    }

    if($('.full_width').length){
      $('.full_width').owlCarousel({
          stopOnHover : true,
          rewindNav:false,
          navigation:true,
          paginationSpeed : 1000,
          goToFirstSpeed : 1000,
          singleItem : true,
          items : 1,
          loop: true,
          smartSpeed:1000,
          mouseDrag:true,
          autoplayTimeout:4000,
      });
      console.log("full_width_owl");
    };

    if($('.else_owl').length){

      $('.else_owl').each(function(){

     
            var itemA = $(this).attr('data-itemA'),
                itemB = $(this).attr('data-itemB'),
                itemC = $(this).attr('data-itemC'),
                itemD = $(this).attr('data-itemD'),
                itemE = $(this).attr('data-itemE'),
                itemF = $(this).attr('data-itemF'),
                itemG = $(this).attr('data-itemG');

          $(this).owlCarousel({
                  nav: true ,
                  mouseDrag:true,
                  paginationSpeed : 1000,
                  goToFirstSpeed : 1000,
                  smartSpeed:500,
                  responsive: {
                      0 : {
                           items: itemG
                      },
                      440 : {
                           items: itemF
                      },
                      620 : {
                           items: itemE
                      },
                      768 : {
                           items: itemD
                      },
                      991 : {
                           items: itemC
                      },
                      1199 : {
                           items: itemB
                      },
                      1900 : {
                           items: itemA
                      }
                  }
          });
          console.log("else_owl");

      });      
    };  

    // popup

    if($(".open_popup").length){
      $(".open_popup").on('click',function(){
          var modal = $(this).data("modal");
          $(modal).arcticmodal();
      });
    };
      
    // popup end


      // menu_responsive start


      // if($(".toggle_menu_btn").length){
      //   $(".toggle_menu_btn").on("click", function(){
      //       $(this).next().slideToggle().toggleClass("active");
      //       $(this).toggleClass("active");
      //   });
    
      //   $(document).on("click ontouchstart", function(event){
      
      //     if($(event.target).closest('.menu_header_absolute ').length || $(event.target).closest('.toggle_menu_btn').length) return;
          
      //       $('.menu_header_absolute ').removeClass("active");
      //       $(".menu_header_absolute ").slideUp();
      //       $('.toggle_menu_btn').removeClass("active");
            
      //     event.stopPropagation()
      //   })
      // }
      $(".toggle_menu_btn").on("click", function(){
        $("body").toggleClass("show_menu");
      })

      
      // menu_responsive end

      //  counter basket start

        if($(".counter_box").length){
          (function(){
            function getSpacePosition(str){

              if(str.length > 6) return str;

              var result = "",
                firstPart = "",
                secondPart,
                currPos;

              for(var j = str.length - 3; j > 0; j -= 3){

                firstPart += str.slice(0, j) + " ";
                secondPart = str.slice(j);

              }

              return firstPart + secondPart;
            }

            $('.counter_number').each(function(){

              var $this = $(this),
                input = $this.children('input'),
                output = $this.closest('div').find('.total_cost');  //,'li div span'
                output.data('price', parseFloat(output.text().split(" ").join(""),10));

                // console.log(output.data('price'));

                $this.on('click','button', function(){

                  var currentVal = +input.val(),
                  cO = parseFloat(output.text().split(" ").join(""),10);
                  direction = $(this).hasClass('counter_minus') ? 'minus' : 'plus';

                  if( +input.val() - 1 == 0 && direction == "minus") return;
           
                  if(direction == "plus"){
                    input.val(++currentVal); 
                    output.text(getSpacePosition(currentVal * output.data('price') + ""));
                  }
                  else{
                    input.val(--currentVal); 
                    output.text(getSpacePosition(cO - output.data('price') + "")); 
                  }
                });
              });
          }());
        };
      //  counter basket end

});



